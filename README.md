# Базовое web приложение на aiohttp c асинхронными запросами в БД запускаемое с помощью docker
## СТЕК:
    * linux
    * python
    * docker
    * docker-compose
    * asincio
    * aiohttp
    * sqlalchemy
    * postgresql

1. Скопируте проект к себе с помощью команды: ```git clone https://gitlab.com/Suvorov-progects/aiohttp-postgresql-sqlalchemi-docker-compose.git```
2. Перейдите в папку с скопированным кодом, например с помощью команды ```cd docker_aiohttp```
3. В корне проекта создать файл ```.env``` в котором в перменной DB указать путь к БД, например ```DB="postgresql+asyncpg://postgres:postgres@localhost/demo"```
4. Для запуска с помощью docker:
    - Выполните команду: ```docker-compose up -d --build```
    - У себя в браузере перейдите по адресу ```localhost:8080```
    - Для остановки работы приложения выполните команду ```docker-compose down```
