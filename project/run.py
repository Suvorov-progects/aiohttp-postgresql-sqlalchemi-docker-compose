from aiohttp import web
import asyncio

from os.path import join, dirname
from dotenv import load_dotenv

from app import create_app


# uvloop работает в несколько раз быстрее чем asyncio, поэтому заменяем на него
try:
    import uvloop
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
except ImportError:
    print('uviloop не сработал')

app = create_app()

if __name__ == '__main__':
    dotenv_path = join(dirname(__file__), '.env')
    load_dotenv(dotenv_path)
    web.run_app(app)
