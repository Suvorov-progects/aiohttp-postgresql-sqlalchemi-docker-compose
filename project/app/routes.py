from .views import views


def setup_routes(app):
    app.router.add_route('GET', '/', views.index)
    app.router.add_route('GET', '/create_user', views.create_user)
    app.router.add_route('GET', '/create_costumers', views.create_costumers)
