from sqlalchemy.orm import declarative_base
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy import MetaData
from sqlalchemy import orm
from sqlalchemy.ext.asyncio import AsyncSession
import os


metadata = MetaData()
Base = declarative_base(metadata=metadata)


class AsyncDatabaseSession:
    def __init__(self):
        self._session = None
        self._engine = None
        self._db = None

    def __getattr__(self, name):
        return getattr(self._session, name)

    async def init(self):
        self._db = os.environ.get("DB")
        self._engine = create_async_engine(self._db, echo=True)
        self._session = orm.sessionmaker(
            self._engine, expire_on_commit=False, class_=AsyncSession
        )()

    async def create_all(self):
        async with self._engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)


async_db_session = AsyncDatabaseSession()
