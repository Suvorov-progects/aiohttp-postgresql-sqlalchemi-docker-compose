from aiohttp import web
import aiohttp_jinja2
import jinja2

from .models import init_app
from .routes import setup_routes


async def create_app():
    app = web.Application()
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.PackageLoader('app', 'templates')
    )
    setup_routes(app)
    # необходим если нет миграций
    # await init_app()
    return app
