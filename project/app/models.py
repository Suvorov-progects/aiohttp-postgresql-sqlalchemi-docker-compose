from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy import update as sqlalchemy_update
from sqlalchemy.future import select
from sqlalchemy.orm import relationship
from . import db


class MethodDB:

    @classmethod
    async def create(cls, **kwargs):
        db.async_db_session.add(cls(**kwargs))
        await db.async_db_session.commit()

    @classmethod
    async def update(cls, id, **kwargs):
        query = (
            sqlalchemy_update(User)
            .where(User.id == id)
            .values(**kwargs)
            .execution_options(synchronize_session="fetch")
        )
        await db.async_db_session.execute(query)
        await db.async_db_session.commit()

    @classmethod
    async def get(cls, id):
        query = select(cls).where(cls.id == id)
        results = await db.async_db_session.execute(query)
        (result,) = results.one()
        return result


class User(db.Base, MethodDB):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    full_name = Column(String)
    posts = relationship("Post")
    # требуется для доступа к столбцам со значениями сервера по умолчанию
    # или значениями выражения SQL по умолчанию после сброса,
    # без запуска загрузки с истекшим сроком действия
    __mapper_args__ = {"eager_defaults": True}

    def __repr__(self):
            return (
                f"<{self.__class__.__name__}("
                f"id={self.id}, "
                f"full_name={self.full_name}, "
                f"posts={self.posts}, "
                f")>"
            )


class Post(db.Base, MethodDB):
    __tablename__ = "posts"

    id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(ForeignKey("users.id"))
    data = Column(String)

    def __repr__(self):
        return (f"<{self.__class__.__name__}, id={self.id}, data={self.data}" f")>")

    @classmethod
    async def filter_by_user_id(cls, user_id):
        query = select(cls).where(cls.user_id == user_id)
        posts = await db.async_db_session.execute(query)
        return posts.scalars().all()


class Costumers(db.Base, MethodDB):
    __tablename__ = "costumers"

    id = Column(Integer, primary_key=True, autoincrement=True)
    costumers = Column(String)
    data = Column(String)

async def init_app():
    await db.async_db_session.init()
    await db.async_db_session.create_all()
