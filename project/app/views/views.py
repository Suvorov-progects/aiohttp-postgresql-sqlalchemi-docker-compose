from aiohttp_jinja2 import template

from app.models import User, Post, Costumers


@template('index.html')
async def index(request):
    return {'name': 'NAME'}


@template('create_user.html')
async def create_user(request):
    await User.create(full_name="John Doe")
    user = await User.get(1)
    return {'user': user.id}


@template('create_post.html')
async def create_post(user_id, data):
    await Post.create(user_id=user_id, data=data)
    posts = await Post.filter_by_user_id(user_id)
    return posts


@template('update_user.html')
async def update_user(id, full_name):
    await User.update(id, full_name=full_name)
    user = await User.get(id)
    return user.full_name


@template('create_costumers.html')
async def create_costumers(request):
    await Costumers.create(costumers="John Doe", data="data")
    costumers = await Costumers.get(1)
    return {'costumers': costumers.id}
